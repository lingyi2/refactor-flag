from turtle import *
from defines import *
#Start of Flag
penup()
forward(300)
left(90)
forward(250)
pendown()



square("black",600,300)
square("blue",600,100 )
move_next()
square("green",600,100 )
move_next()
square("cyan",600,100 )


penup()
forward(50)
left(90)
forward(300)
right(180)
pendown()
star("yellow",100)
#End of First Star
next_star()
#Beginning Second Star
star("blue",75)
#End of Second Star
next_star()
#Start of Third Star
star("green",50)
#End of Third Star
next_star()
#Start of Final Star
star("cyan",25)
#End of Final Star

#Start of "Peace"
penup()
backward(250)
right(90)
forward(35)
right(90)
pendown()
words("black", "Peace", 30)
#End of Peace
#Start of Love
penup()
backward(400)
pendown()
words("black", "Love", 30)
#End of Love
#Start of input
penup()
left(90)
forward(100)
right(90)
forward(180)
pendown()

for count in range(1):
    name = textinput("Name", "Who is the ruler of the country?")
    words("black", name, 20)

done()
