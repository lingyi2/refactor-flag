from turtle import *

def square(square_color,length, width):
    speed(10)
    color(square_color)
    begin_fill()
    left(90)
    forward(length)
    left(90)
    forward(width)
    left(90)
    forward(length)
    left(90)
    forward(width)
    end_fill()

def move_next():
    penup()
    backward(100)
    pendown()

def star(star_color, size):
    begin_fill()
    for count in range(5):
        color(star_color)
        forward(size)
        right(144)
        forward(size)
    end_fill()

def next_star():
    penup()
    right(90)
    forward(5)
    left(90)

def words(words_color, words, size):
    color(words_color)
    write(words, font=("Times New Roman", size, "normal"))
